/**
 * Created by user on 21.11.16.
 */
package com.tutorialspoint;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@Controller
public class HelloController {

    @RequestMapping("/")
    public String printHello(ModelMap model) {
        model.addAttribute("message", "Hello Spring MVC Framework!");
        return "hello";
    }

    @RequestMapping(value = "/metanit/Classes/{fileName:.+}")
    public ModelAndView method(@PathVariable("fileName") String fileName) {
        return new ModelAndView("redirect:" + "/resources/js/metanit/2/Classes/"+fileName);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public String loginMethod(HttpServletRequest request) {
        Map map = request.getParameterMap();
        System.out.println(map);
        return "{\"success\": true, \"message\": \"привет\" }";
    }

    @RequestMapping("/metanit/{folder:.+}/{file:.+}")
    public ModelAndView printMetanitAny(@PathVariable("folder") String folder, @PathVariable("file") String file, ModelAndView model) {
        if (folder.equals("BookApp") && (!file.equals("1"))) {
            return new ModelAndView("redirect:" + "/resources/js/metanit/BookApp/app/model/"+file);
        }
        model.addObject("Title", "Hello Spring MVC Framework!");
        model.addObject("Folder", folder);
        model.addObject("File", file);
        model.setViewName("metanit");
        return model;
    }

    @RequestMapping("/json/{file:.+}")
    public ModelAndView jsonMethod(@PathVariable("file") String fileName) {
        return new ModelAndView("redirect:" + "/resources/js/metanit/json/"+fileName);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
//    @ResponseBody
    public String uploadMethod(MultipartFile multipartFile) {
        return "{\"success\": true, \"file\": \"привет\" }";
    }
}