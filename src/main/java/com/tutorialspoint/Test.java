package com.tutorialspoint;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.*;

/**
 * Created by user on 21.12.16.
 */
public class Test {
    public static void main(String[] args) {
        String input = "[{\"44637\":{\"id\":44637,\"url_photo\":\"http:\\/\\/www.petshop.ru\"}}]";
        List<Map<String, Object>> list = parseJson(input);
        System.out.println("list = " + list);
//
        List<Map<String, Object>> listMap = new ArrayList<>();
        Map<String, Object> subMap = new HashMap<>();
        subMap.put("name", "test");
        subMap.put("surname", "surtest");

        Map<String, Object> mainMap = new HashMap<>();
        mainMap.put("123", subMap);

        listMap.add(mainMap);
        Gson gson = new Gson();
        String str = gson.toJson(listMap);
        System.out.println("str = " + str);

        Person pers = new Person(1, "tester");
        Map<String, Object> jsonPersonMap = new HashMap<>();
        jsonPersonMap.put("success", true);
        List<Person> persons = new ArrayList<>(Arrays.asList(pers));
        jsonPersonMap.put("users", persons);
        System.out.println("jsonPersonMap = " + gson.toJson(jsonPersonMap));

        System.out.println("persons.size() = " +persons.size());
        persons.remove(0);
        System.out.println("persons.size() = " +persons.size());

        /*****/
        int pdv = 30;
        double dec =  pdv/100.0;
        System.out.println(dec);


    }

    public static List<Map<String, Object>> parseJson(String data) {
        Gson gson = new Gson();
        return gson.fromJson(data, new TypeToken<List<Map<String, Object>>>() {
        }.getType());
    }

    static class Person{
        private int id;
        private String name;
        Person(){}
        Person(int id, String name){
            this.id = id;
            this.name = name;
        }
    }
}
