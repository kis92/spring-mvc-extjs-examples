package com.tutorialspoint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@Controller
@RequestMapping(value = "/grid/users")
public class GridController {
    private Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
    private List<Person> persons = new ArrayList<>();
    private boolean firstLoad = true;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String readAll() {
        if(firstLoad){
            Person person = new Person();
            person.setUserId(0);
            person.setDate(new Date());
            person.setMarried(false);
            person.setName("TestName");
            person.setSurname("TestSurname");
            person.setEmail("qwe@qwe.rt");
            persons.add(person);
            firstLoad = false;
        }

        Map<String, Object> jsonPersonMap = new HashMap<>();
        jsonPersonMap.put("success", true);
        jsonPersonMap.put("users", persons);
        return gson.toJson(jsonPersonMap);
    }

    @RequestMapping(value = "/{idUser:.+}", method = RequestMethod.PUT)
    @ResponseBody
    public String update(@RequestBody String input) {
        Person pers = gson.fromJson(input,Person.class);
        Person inner = persons.get(pers.getUserId());
        if(inner != null){
            inner.setEmail(pers.getEmail());
            inner.setName(pers.getName());
            inner.setMarried(pers.getMarried());
            inner.setSurname(pers.getSurname());
            inner.setDate(pers.getDate());
        }
        return readAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String create(@RequestBody String input) {
        System.out.println("input = " + input);
        Person pers = gson.fromJson(input,Person.class);
        if(pers.getUserId()==0){
            pers.setUserId(persons.size());
            pers.setDate(null);
            pers.setSurname(null);
            pers.setName(null);
            pers.setEmail(null);
            pers.setMarried(null);
        }
        persons.add(pers);
        return readAll();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@RequestBody String input) {
        Person pers = gson.fromJson(input,Person.class);
        persons.remove(pers);
        //return readAll();
    }

    private class Person {
        private Integer userId;
        private String name, surname;
        private Date date;
        private String email;
        private Boolean married;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public Boolean getMarried() {
            return married;
        }

        public void setMarried(Boolean married) {
            this.married = married;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }
    }
}