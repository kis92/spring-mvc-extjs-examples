package com.tutorialspoint;

import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 22.12.16.
 */

/*
{
  "success": true,
  "books": [
    {"id": 1, "name": 'Война и мир', "author": 'Л. Толстой', "year": 1863},
    {"id": 2, "name": 'Отцы и дети', "author": 'И. Тургенев', "year": 1862},
    {"id": 3, "name": 'Евгений Онегин', "author": 'А. Пушкин', "year": 1825}
  ]
}
 */

@Controller
public class BookController {
    private Gson gson = new Gson();
    private List<Book> books = new ArrayList<>();
    private Map<String, Object> mapForJson = new HashMap<>();
    private boolean firstLoad = true;

    @RequestMapping(value = "/metanit/BookApp/{folderName:.+}/{fileName:.+}")
    public ModelAndView method(
            @PathVariable("folderName") String folderName, @PathVariable("fileName") String fileName
    ) {
        return new ModelAndView("redirect:" + "/resources/js/metanit/BookApp/app/" + folderName + "/" + fileName);
    }

    @RequestMapping(value = "/data", method = RequestMethod.GET)
    @ResponseBody
    public String readAll() {
        if(firstLoad) {
            Book warPeace = new Book(0, "Война и мир", "Л. Толстой", 1863);
            books.add(warPeace);
            Book fathersSons = new Book(1, "Отцы и дети", "И. Тургенев", 1862);
            books.add(fathersSons);
            Book onegin = new Book(2, "Евгений Онегин", "А. Пушкин", 1825);
            books.add(onegin);
            firstLoad = false;
        }
        mapForJson.put("success", "true");
        mapForJson.put("message", "");
        mapForJson.put("books", books);
        String res = gson.toJson(mapForJson);
        return res;
    }

    @RequestMapping(value = "/data/update", method = RequestMethod.PUT)
    @ResponseBody
    public String update(@RequestBody String input) {
        Book inputBook = gson.fromJson(input, Book.class);
        books.add(inputBook.getId(), inputBook);
        mapForJson.put("message","книга обновлена");
        return gson.toJson(mapForJson);
    }

    @RequestMapping(value = "/data/create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@RequestBody String input) {
        System.out.println("input = " + input);
        Book inputBook = gson.fromJson(input, Book.class);
        if(inputBook.getId() == null){
            inputBook.setId(books.size());
        }
        books.add(inputBook);
        mapForJson.put("message","книга добавлена");
        return gson.toJson(mapForJson);
    }

    @RequestMapping(value = "/data/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@RequestBody String input) {
        System.out.println("idBook = " + input);
        books.remove(Integer.parseInt(input));
        mapForJson.put("message","книга удалена");
        return gson.toJson(mapForJson);
    }

    class Book {
        private Integer id;
        private String name;
        private String author;
        private int year;

        public Book() {
        }

        public Book(int id, String name, String author, int year) {
            this.id = id;
            this.name = name;
            this.author = author;
            this.year = year;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        @Override
        public String toString() {
            return "Book{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", author='" + author + '\'' +
                    ", year=" + year +
                    '}';
        }
    }
}
