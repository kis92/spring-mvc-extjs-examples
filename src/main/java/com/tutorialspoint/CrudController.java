/**
 * Created by user on 21.11.16.
 */
package com.tutorialspoint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Controller
public class CrudController {
    private Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy").create();
    private static Map<Integer, Person> persons = new HashMap<>();
    static {
        Person person = new Person();
        person.setPersonId(1);
        person.setBirthDate(new Date());
        person.setMarried(false);
        person.setName("TestName");
        person.setSurname("TestSurname");
        person.setSalary(1000);
        persons.put(1, person);
    }


    @RequestMapping(value = "/read")
    @ResponseBody
    public String read(@RequestParam("id") int id) {
        Person pers = persons.get(id);
        return gson.toJson(pers);
    }

    @RequestMapping(value = "/readAll")
    @ResponseBody
    public String readAll() {
        return gson.toJson(persons);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody String jsonPerson) {
        Person person = gson.fromJson(jsonPerson, Person.class);
        if(person != null) {
            persons.put(person.getPersonId(), person);
        }
        return gson.toJson(jsonPerson);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public String create(@RequestBody String jsonPerson) {
        Person person = gson.fromJson(jsonPerson, Person.class);
        int count = persons.size()+1;
        if(person!= null) {
            person.setPersonId(count);
            persons.put(count, person);
            return gson.toJson(person);
        }
        return gson.toJson(jsonPerson);
    }

    @RequestMapping(value = "/delete")
    @ResponseBody
    public String delete(@RequestBody String jsonPerson) {
        Person person = gson.fromJson(jsonPerson, Person.class);
        person = persons.remove(person.getPersonId());
        return gson.toJson(person);
    }

    @RequestMapping(value = "/addPerson", method = RequestMethod.PUT)
//    @ResponseStatus(HttpStatus.OK) http://www.baeldung.com/building-a-restful-web-service-with-spring-and-java-based-configuration
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public Person addPerson(@RequestBody Person person){
        return person;
    }

    @RequestMapping(value = "/getPerson", method = RequestMethod.GET)
    @ResponseBody
    public Person getPerson(){
        return new Person("qwe","qwe");
    }

    private static class Person{
        private Integer personId, salary;
        private String name,surname;
        private Boolean married;
        private Date birthDate;

        public Person(){
        }

        public Person(String name, String surname){
            this.name = name;
            this.surname = surname;
        }

        public Integer getPersonId() {
            return personId;
        }

        public void setPersonId(Integer personId) {
            this.personId = personId;
        }

        public Integer getSalary() {
            return salary;
        }

        public void setSalary(Integer salary) {
            this.salary = salary;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public Boolean getMarried() {
            return married;
        }

        public void setMarried(Boolean married) {
            this.married = married;
        }

        public Date getBirthDate() {
            return birthDate;
        }

        public void setBirthDate(Date birthDate) {
            this.birthDate = birthDate;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "personId=" + personId +
                    ", salary=" + salary +
                    ", name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", married=" + married +
                    ", birthDate=" + birthDate +
                    '}';
        }
    }
}