<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%--
animate
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>${Title}</title>
    <link href="<c:url value="/resources/css/css/ext-all.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/ext4/ext-all-dev.js" />"></script>

    <script>
      Ext.onReady(function () {
        var element = Ext.get('content');
//        element.on('click', function (e, target, options) {
//          console.log('element was pushed');
//        }, this);
        element.on({
          click: function (e, target, option) {
            console.log('was left click');
          },
          contextmenu:function (e, target, option) {
            console.log('was right clicked');
          },
          scope:this
        });

        Ext.create('Ext.Button',{
          margin:'10 0 0 30',
          text:'push me',
          renderTo:Ext.getBody(),
          listeners:{
            click:function () {
              console.log('button click');
            },
            scope:this
          }
        });

        var menu = Ext.get('menu');
        menu.on('click', function (e, target, option) {
          if(e.getTarget('li .buy')){
            console.log('buy');
          }else if(e.getTarget('li .sell')){
            console.log('sell');
          }else if(e.getTarget('li .exit')){
            console.log('exit');
          }
        }, this, {delegate:'li'});


        Ext.create('Ext.fx.Animator', {
          target: 'jump',
          duration: 4000,
          keyframes: {
            0: {
              y: 100
            },
            20: {
              y: 250,
              backgroundColor: 'yellow'
            },
            40: {
              y: 155,
              x:110,
              backgroundColor: '#0000FF'
            },
            60: {
              y: 250,
              x:120
            },
            80: {
              y: 225,
              x:170,
              backgroundColor: 'red'
            },
            100: {
              y: 250,
              x:180
            }
          }
        });

      });
    </script>

    <style>
      #jump{
        y: 100px;
        x: 100px;
        width: 60px;
        height: 60px;
        border-radius: 30px;
        background-color: red;
        position: absolute;
      }
    </style>
  </head>
  <body>
  <div id="jump"></div>
  <div id="content">привет мир</div>
    <ul id="menu">
      <li class="buy">buy home</li>
      <li class="sell">sell soul</li>
      <li class="exit">exit of ipoteka</li>
    </ul>
  </body>
</html>
