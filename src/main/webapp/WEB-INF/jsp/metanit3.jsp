<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%--
  DOM manipulations
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>${Title}</title>
    <link href="<c:url value="/resources/css/css/ext-all.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/ext4/ext-all-dev.js" />"></script>
    <%--<script src="<c:url value="/resources/js/metanit/3/app7.js" />"></script>--%>

    <script>Ext.onReady(function () {
      var elements = Ext.select('div.class1').item(0);
      elements.hide();
      console.log(elements.dom.innerHTML);
      console.log(elements.next().dom.innerHTML);
      console.log(elements.prev().dom.innerHTML);

      var element = Ext.get('context');
      console.log(element.first().dom.innerHTML);
      console.log(element.last().dom.innerHTML);
      console.log(element.last().parent().dom.innerHTML);
      console.log(element.last().parent().child('div#text2').dom.innerHTML);

      Ext.core.DomHelper.append(element, '<h4>h4-h4</h4>');
      var newOlLi = {
        tag:'ol',
        cls:'listClass',
        children:[
          {
            tag:'li',
            html:'first'
          },
          {
            tag:'li',
            html:'second'
          }
        ]
      };
      Ext.core.DomHelper.append(element, newOlLi);

      var temp = Ext.DomHelper.createTemplate({
        tag:'h4',
        html:'{header}'
      });
      temp.append(element, {header:'123qwe'});


      var el = Ext.get('text1');
      el.setStyle({
        fontFamily:'Verdana',
        fontSize:'13px',
        backgroundColor: 'silver'
      });
      el.update('<b>hello world</b>');



    })</script>
  </head>
  <body>
    <div id="context">
      <div id="text1">id one</div>
      <div class="class1">class one</div>
      <div id="text2">id two</div>
      <div class="class2">class two</div>
      context text
    </div>
  </body>
</html>
