<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 21.11.16
  Time: 14:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>${Title}</title>
    <link href="<c:url value="/resources/css/css/ext-all.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/ext4/ext-all-dev.js" />"></script>
    <script src="<c:url value="/resources/js/metanit/${Folder}/app${File}.js" />"></script>
  </head>
  <body>
  </body>
</html>
