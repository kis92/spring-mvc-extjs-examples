Ext.define('BookApp.model.BookModel',{
    extend:Ext.data.Model,
    fields:['id','name','author','year']
});