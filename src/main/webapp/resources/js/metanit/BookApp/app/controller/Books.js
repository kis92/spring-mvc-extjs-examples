Ext.define('BookApp.controller.Books', {
    extend: Ext.app.Controller,
    views: ['BookApp.view.Book', 'BookApp.view.BookList'],
    stores: ['BookApp.store.BookStore'],
    models: ['BookApp.model.BookModel'],
    init: function () {
        this.control({
            'viewport > booklist': {itemdblclick: this.editBook},
            'bookwindow button[action=new]': {click: this.createBook},
            'bookwindow button[action=save]':{click: this.updateBook},
            'bookwindow button[action=delete]':{click: this.deleteBook},
            'bookwindow button[action=clear]':{click:this.clearForm}
        });
    },

    updateBook:function (button) {
        var win = button.up('window'),
            form = win.down('form'),
            values = form.getValues(),
            id = form.getRecord().get('id');
            values.id = id;
        Ext.Ajax.request({
            url:'/data/update',
            method: 'PUT',
            //params:values,
            params: Ext.JSON.encode(values),
            success: function (response) {
                win.close();
                var data = Ext.decode(response.responseText);
                if (data.success){
                    var store = Ext.widget('booklist').getStore();
                    store.load();
                    Ext.Msg.alert('Обновление', data.message);
                }else{
                    Ext.Msg.alert('Обновление','Не удалось обновить книгу в библиотеке');
                }
            }
        });
    },

    createBook:function (button) {
        var win = button.up('window'),
            form = win.down('form'),
            values = form.getValues();
        Ext.Ajax.request({
            url:'/data/create',
            method: 'POST',
            params: Ext.JSON.encode(values),
            success:function (response) {
                win.close();
                var data = Ext.decode(response.responseText);
                if(data.success){
                    Ext.Msg.alert('Создание', data.message);
                    var store = Ext.widget('booklist').getStore();
                    store.load();
                }else{
                    Ext.Msg.alert('Создание','Не удалось добавить книгу в библиотеку');
                }
            }
        });
    },

    deleteBook:function (button) {
        var win = button.up('window'),
            form = win.down('form'),
            id = form.getRecord().get('id');
        Ext.Ajax.request({
            url:'/data/delete',
            method: 'DELETE',
            params:id,
            success: function (response) {
                win.close();
                var data = Ext.decode(response.responseText);
                if(data.success){
                    Ext.Msg.alert('Удаление',data.message);
                    var store = Ext.widget('booklist').getStore();
                    store.load();
                    // var record = store.getById(id);
                    //store.remove(record);
                    //form.getForm().reset();
                }else{
                    Ext.Msg.alert('Удаление','Не удалось удалить книгу из библиотеки');
                }
            }
        });
    },
    
    clearForm: function (grid, record) {
        var view = Ext.widget('bookwindow');
        view.down('form').getForm().reset();
    },
    
    editBook: function (grid, record) {
        var view = Ext.widget('bookwindow');
        view.down('form').loadRecord(record);
    }
    
});