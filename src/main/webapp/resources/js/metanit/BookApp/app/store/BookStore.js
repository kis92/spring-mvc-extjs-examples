Ext.define('BookApp.store.BookStore',{
    extend:Ext.data.Store,
    model:'BookApp.model.BookModel',
    autoLoad:true,
    storeId:'BookStore',
    proxy:{
        type:'ajax',
        // url:'/data/books.json',
        url:'/data',
        reader:{
            type:'json',
            root:'books',
            successProperty:'success'
        },
        writer: {
            type: 'json',
            encode: false
        }
    }
});