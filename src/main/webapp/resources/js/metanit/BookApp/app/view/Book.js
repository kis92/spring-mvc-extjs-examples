Ext.define('BookApp.view.Book',{
    extend: 'Ext.window.Window',
    alias:'widget.bookwindow',
    title:'BOOK',
    layout:'fit',
    autoShow:true,
    initComponent: function () {
        this.items = [{
            xtype:'form',
            items:[
                {
                    xtype:'textfield',
                    name:'name',
                    fieldLabel:'Composition',
                    allowBlank : false
                },
                {
                    xtype:'textfield',
                    name:'author',
                    fieldLabel:'Author',
                    allowBlank : false
                },
                {
                    xtype:'numberfield',
                    name:'year',
                    fieldLabel:'Year',
                    minValue:1,
                    allowBlank : false
                }
            ]
        }];
        this.dockedItems = [{
            xtype:'toolbar',
            docked:'top',
            items:[
                {
                    text:'Create',
                    iconCls:'new-icon',
                    action:'new'
                },
                {
                    text:'Save',
                    iconCls:'save-icon',
                    action:'save'
                },
                {
                    text:'Delete',
                    iconCls:'delete-icon',
                    action:'delete'
                }
            ]
        }];
        this.buttons = [{
            id:'clearButton',
            text:'Clear',
            scope:this,
            action:'clear'
        }];

        this.callParent(arguments);
    }
});