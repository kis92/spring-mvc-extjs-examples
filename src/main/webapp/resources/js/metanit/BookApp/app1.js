// включаем динамическую загрузку JS файлов
Ext.Loader.setConfig({
    enabled: true
    // ,
    // disableCaching: false,
    ,paths: {BookApp: '/metanit/BookApp'}
});

// указываем зависимости, которые необходимо предварительно загрузить
Ext.require([
    "BookApp.controller.Books",
    "BookApp.view.Book",
    "BookApp.view.BookList",
    "BookApp.store.BookStore",
    "BookApp.model.BookModel"
]);


Ext.application({
    requires:[Ext.container.Viewport],
    name:'BookApp',
    // appFolder:'BookApp',
    controllers:['Books'],
    launch: function () {
        new Ext.container.Viewport({
            // layout:'fit',
            id:'viewport',
            items:[
                {
                    xtype:'booklist'
                },
                {
                    xtype:'button',
                    text:'Add book',
                    handler: function (button) {
                        var bookWindow = new BookApp.view.Book();
                        var bookWindowMenu = bookWindow.dockedItems.items[1].items;
                        bookWindowMenu.items[1].hide();//update
                        bookWindowMenu.items[2].hide();//delete
                        bookWindow.dockedItems.items[2].items.items[0].hide();//clear
                        bookWindow.show();
                    }
                }
            ]
        });
    }
});