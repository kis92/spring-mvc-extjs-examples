/*
* Объявление класса с config : автоматически создаются методы get/set reset and apply
* apply calling after called setter
* */

Ext.define('Classes.Person', {
    config : {
        name: 'Vasya',
        surname: 'Pupkin'
    },

    constructor: function (config) {
       this.initConfig(config)
    },

    applySurname: function (surname) {
        surname = surname.replace(/\s/g,'');
        if(surname.length==0){
            alert('surname is empty');
        }else{
            return surname;
        }
    },

    getInfo: function () {
        alert("full name : " + this.getName() + " " + this.getSurname());
    }
});

var vasya = new Classes.Person;
//vasya.getInfo();
vasya.setSurname(" ");
vasya.getInfo();

var test = new Classes.Person({name:"123", surname:"321"});
test.getInfo();