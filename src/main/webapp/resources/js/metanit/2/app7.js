Ext.define('Classes.Person',{
    alias: 'person',
    name : 'test',
    constructor: function (name) {
        if(name){
            this.name = name;
        }
    },
    getInfo: function () {
        console.log("name = " + this.name);
    }
});

var test = new Classes.Person();
test.getInfo();

Ext.define('Person.Panel', {
    alias: 'widget.personpanel',
    extend: 'Ext.panel.Panel',
    title: 'Ext Person Panel',
    html: 'new panel'
});

Ext.application({
    name : "hiExt",
    launch: function () {
        Ext.create('Ext.container.Viewport',{
            layout:'fit',
            items:[
                {
                    xtype:'personpanel'
                }
            ]
        })
    }
});
