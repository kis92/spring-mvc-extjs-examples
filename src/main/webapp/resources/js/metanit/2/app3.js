/*static members class*/
Ext.define('Classes.Person', {
    config: {
        name: 'Eugene',
        surname : 'Popov'
    },
    statics: {
        instanceCount: 0,
        // статический метод, возвращающий объект класса
        factory: function(name, surname) {
            return new this({name: name, surname: surname});
        }
    },
    constructor: function(config) {
        this.initConfig(config);
        // свойство 'self' ссылается на класс объекта
        this.self.instanceCount ++;
    },
    getinfo: function()
    {
        alert("Полное имя : " + this.name + " " + this.surname);
    },
});

var eugene = Ext.create('Classes.Person');
eugene.getinfo();
var james = Classes.Person.factory('James', 'Gosling');
alert(Classes.Person.instanceCount);
james.getinfo();

/*
 Здесь в секции static мы объявляем, во-первых, статическую переменную instanceCount, которая будет обозначать количество созданных объектов данного класса. И данная переменная будет общей для всех объектов этого класса.

 Во-вторых, объявляется статическая функция factory создает объект данного класса.

 В конструкторе мы обращаемся к классу текущего объекта и увеличиваем переменную на единицу: this.self.instanceCount ++

 В итоге при обращении к статическим членам объекта нам надо будет указывать имя класса, а не имя объекта: alert(Classes.Person.instanceCount);
*/