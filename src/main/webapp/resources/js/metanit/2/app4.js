/*Наследование*/
Ext.define('Person', {
    config: {
        name: 'Jk',
        surname: 'Test'
    },
    constructor: function (name, surname) {
        this.initConfig();
        if (name) {
            this.setName(name);
        }
        if (surname) {
            this.setSurname(surname);
        }
    },
    getInfo: function () {
        alert("Full name : " + this.name + " " + this.surname);
    }
});

Ext.define('Manager', {
    extend: 'Person',
    config: {department: 'sales'},
    constructor: function (name, surname, department) {
        this.initConfig();
        if(department){
            this.setDepartment(department);
        }
        this.callParent([name, surname]);
    },
    getInfo: function () {
        this.callParent();
        alert("Department : " + this.getDepartment());
    }
});

var test = new Manager("qwe","rty","manager");
test.getInfo();