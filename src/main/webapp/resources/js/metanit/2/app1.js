/*
* Простое объявление класса
* */

Ext.define('Classes.Person', {
    name: 'Vasya',
    surname: 'Pupkin',

    constructor: function (name, surname) {
        if (name && surname) {
            this.name = name;
            this.surname = surname;
        }
    },

    getInfo: function () {
        alert("full name : " + this.name + " " + this.surname);
    }
});

var vasya = new Classes.Person;
vasya.getInfo();
vasya.surname = "Test";
vasya.getInfo();

var test = new Classes.Person("123", "321");
test.getInfo();