//mixin classes pattern composition
Ext.define('Classes.Car', {
    speed: 0,
    drive : function (speed) {
        if(speed > 0){
            console.log("cars is runned");
        }else if(speed == 0){
            console.log("cars is stopped");
        }else{
            console.log("cars is reverse");
        }
    }
});

Ext.define('Classes.Person', {
    mixins:{
        car : 'Classes.Car'
    },
    config : {
        name : 'Name',
        surname : 'Surname'
    },
    constructor : function (name, surname) {
        this.initConfig();
        if(name){
            this.setName(name);
        }
        if(surname){
            this.setSurname(surname);
        }
    },
    go : function (speed) {
        this.drive(speed);
    }
});

var test = new Classes.Person();
test.go(50);
test.go(0);
test.go(-2);