Ext.onReady(function () {
    Ext.define('Language', {
        extend: 'Ext.data.Model',
        fields: [{
            type: 'string',
            name: 'id'
        }, {
            type: 'string',
            name: 'name'
        }]
    });

    var store = new Ext.data.JsonStore({
        model: 'Language',
        proxy: {
            reader: {
                type: 'jsonp'
            }
        },
        queryMode:'local'
    });

    Ext.Ajax.request({
        url:'/json/9-1.json',
        success: function (response, option) {
            var objAjax= Ext.decode(response.responseText);

            panel.getComponent('txtName').setValue(objAjax.firstname);
            panel.getComponent('txtSurname').setValue(objAjax.lastname);
            panel.getComponent('txtCompany').setValue(objAjax.company);
            store.loadData(objAjax.products);
        },
        failure: function (response, option) {
            console.log('failure:'+response.statusText)
        }
    });

    var panel = new Ext.Panel({
        title:'ajax example',
        width:'99%',
        height:'50%',
        padding:10,
        bodyPadding:5,
        renderTo:Ext.getBody(),
        items:[
            {
                xtype:'textfield',
                fieldLabel:'sur name',
                id:'txtSurname'
            },
            {
                xtype:'textfield',
                fieldLabel:'name',
                id:'txtName'
            },
            {
                xtype:'textfield',
                fieldLabel:'company',
                id:'txtCompany'
            },
            {
                xtype: 'combobox',
                fieldLabel: 'Выберите язык',
                store:store,
                valueField:'myId',
                displayField:'myText',
                queryMode:'local'
            }
        ]
    });
});