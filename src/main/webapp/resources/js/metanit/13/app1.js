Ext.onReady(function () {
    Ext.define('User', {
        extend: Ext.data.Model,
        idProperty: 'userId',
        fields: [
            {
                name: 'userId',
                type: Ext.data.Types.INTEGER
            },
            {
                name: 'name',
                type: Ext.data.Types.STRING
            },
            {
                name: 'surname',
                type: Ext.data.Types.STRING
            },
            {
                name: 'date',
                type: Ext.data.Types.DATE,
                format: 'd/m/Y',
                dateFormat: 'd/m/Y',
                submitFormat: 'd/m/Y',
                altFormats: 'd/m/Y'
            },
            {
                name: 'email',
                type: Ext.data.Types.STRING
            },
            {
                name: 'married',
                type: Ext.data.Types.BOOLEAN
            }
        ]
        ,
        proxy: {
            type: 'rest',
            url: '/grid/users',
            reader: {
                type: 'json',
                root: 'users'
            },
            writer: {
                type: 'json'
            }
        }
    });

    var store = new Ext.data.Store({
        model: User,
        autoLoad: true,
        autoSync: true
        // ,
        // proxy: {
        //     type: 'rest',
        //     url: '/grid/users',
        //     reader: {
        //         type: 'json',
        //         root: 'users'
        //     },
        //     writer: {
        //         type: 'json',
        //         encode: true
        //     }
        // }
    });

    Ext.create('Ext.grid.Panel', {
        title: 'Пользователи',
        id: 'UsersGrid',
        // autoHeight: true,
        height: '55%',
        width: '80%',
        store: store,
        selType: 'rowmodel',
        autoScroll:true,
        plugins: [{
            ptype: 'cellediting',//cellediting
            clicksToEdit: 2,
            autoCancel: false
        }],
        columns: [
            {
                xtype: 'rownumberer',
                dataIndex: 'userId'
            },
            {
                header: 'Имя',
                dataIndex: 'name',
                flex: 1,
                xtype: 'templatecolumn',
                tpl: '<b>{name} {surname}</b>'
            },
            {
                header: 'Аватар',
                dataIndex: 'userId',
                renderer: function (v) {
                    return '<img src="' + v.toString() + '.png"  alt="' + v.toString() + '.png"/>';
                },
                flex: 1
            },
            // {
            //     header: 'Фамилия',
            //     dataIndex: 'surname',
            //     flex: 1
            // },
            {
                header: 'Дата рождения',
                dataIndex: 'date',
                xtype: 'datecolumn',
                format: 'd/m/Y',
                dateFormat: 'd/m/Y',
                submitFormat: 'd/m/Y',
                altFormats: 'd/m/Y',
                flex: 1
                ,
                editor: {
                    xtype: 'datefield',
                    allowBlank: false,
                    format: 'd/m/Y',
                    dateFormat: 'd/m/Y',
                    submitFormat: 'd/m/Y',
                    altFormats: 'd/m/Y'
                }
            },
            {
                header: 'E-mail',
                dataIndex: 'email',
                flex: 1
                ,
                editor: {
                    xtype: 'textfield',
                    allowBlank: false,
                    vtype: 'email'
                }
            },
            {
                header: 'Женат/Замужем',
                dataIndex: 'married',
                flex: 1,
                xtype: 'booleancolumn',
                trueText: 'Yes',
                falseText: 'No',
                renderer: function (v, m) {
                    var fontweight = v ? 'normal' : 'bold';
                    v = v ? 'Да' : 'Нет';
                    m.style = 'font-weight:' + fontweight;
                    return v;
                }
                ,
                editor: {
                    xtype: 'checkbox',
                    checked: false
                }
            },
            {
                xtype: 'actioncolumn',
                width: 40,
                items: [
                    {
                        icon: 'icon-error.gif',
                        handler: function (grid, rowIndex, colIndex) {
                            var store = grid.getStore();
                            var user = store.getAt(rowIndex);
                            store.remove(user);
                            //user.destroy();
                        }
                    }
                ]
            }
        ],
        renderTo: Ext.getBody(),
        buttons: [
            {
                text: 'add user',
                handler: function () {
                    var grid = Ext.getCmp('UsersGrid');
                    var cellEditor = grid.plugins[0];
                    var store = grid.getStore();
                    var newUser = new User();
                    newUser.save();
                    var task = new Ext.util.DelayedTask(function(){
                        store.load();
                    });
                    task.delay(500);
                }
            }
        ]
    });
});