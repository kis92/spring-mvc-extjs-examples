Ext.onReady(function () {
    Ext.create('Ext.panel.Panel', {
        renderTo : Ext.getBody(),
        width:'50%',
        height:'50%',
        padding:10,
        title:'main container',
        items:[
            {
                xtype:'panel',
                title:'panel 1',
                height:50,
                width:'100%'
            },
            {
                xtype:'panel',
                title:'panel 2',
                height:50,
                width:'100%'
            }
        ]
    });
});