Ext.onReady(function () {
    Ext.create('Ext.Panel', {
        title:'Accordion table',
        width:200,
        height:200,
        layout:'accordion',
        renderTo:Ext.getBody(),
        items:[
            {
                xtype:'panel',
                title:'Tolstoy',
                html:'world and piace',
                border:true,
                flex:2
            },
            {
                xtype:'panel',
                title:'Turgenev',
                html:'sonata',
                border:true,
                flex:2
            }
        ]
    })
});