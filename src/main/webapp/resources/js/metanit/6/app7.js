Ext.onReady(function(){
    var panel= Ext.create('Ext.Panel', {
        title: 'Компоновка Anchor',
        width: '50%',
        height: '50%',
        layout:'anchor',
        items: [
            {
                xtype: 'panel',
                title: 'Л. Толстой',
                html: 'Произведения Л. Толстого: "Война и мир", "Воскресение", "Крейцерова соната"',
                anchor: '85% 85%'
            }],
        renderTo: Ext.getBody()
    });
});