Ext.onReady(function () {
    Ext.create('Ext.Panel',{
        title:'table',
        width:'75%',
        height:'75%',
        padding: 10,
        layout:'column',
        items:[
            {
                xtype:'panel',
                title:'one',
                html:'cell 1',
                width:100
            },
            {
                xtype:'panel',
                title:'two',
                html:'cell 2',
                columnWidth:0.5
            },
            {
                xtype:'panel',
                title:'three',
                html:'cell 3',
                columnWidth:0.5
            },
            {
                xtype:'panel',
                title:'four',
                html:'cell 4',
                columnWidth:0.25
            },
            {
                xtype:'panel',
                title:'five',
                html:'cell 5',
                columnWidth:0.25
            },
            {
                xtype:'panel',
                title:'six',
                html:'cell 6',
                columnWidth:0.25
            }
        ],
        renderTo: Ext.getBody()
    });
});