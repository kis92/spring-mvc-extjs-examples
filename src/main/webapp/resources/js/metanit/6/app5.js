Ext.onReady(function () {
    Ext.create('Ext.Panel',{
        width:'75%',
        height:'75%',
        padding:10,
        layout:'border',
        renderTo:Ext.getBody(),
        items:[
            {
                xtype:'panel',
                title:'central panel',
                html: 'panel of center',
                region: 'center',
                margin:'5 5 5 5'
            },
            {
                xtype:'panel',
                title:'north panel',
                html: 'panel of north',
                region: 'north',
                height:'20%',
                split:true
            },
            {
                xtype:'panel',
                title:'south panel',
                html: 'panel of south',
                region: 'south',
                height:'20%',
                titleCollapse: true,
                collapsible: true,
                collapsed: true
            },
            {
                xtype:'panel',
                title:'west panel',
                html: 'panel of west',
                region: 'west',
                width:'30%'
            },
            {
                xtype:'panel',
                title:'east panel',
                html: 'panel of east',
                region: 'east',
                width:'30%'
            }
        ]
    });
});