Ext.onReady(function () {
    var panel = Ext.create('Ext.Panel', {
        title:'slides',
        width:'75%',
        height:'75%',
        layout:'card',
        renderTo:Ext.getBody(),
        items:[
            {
                xtype:'panel',
                title:'fisrt',
                html:'bla-bla-one'
            },
            {
                xtype:'panel',
                title:'second',
                html:'bla-bla-two'
            },
            {
                xtype:'panel',
                title:'third',
                html:'bla-bla-three'
            }
        ],
        bbar:['->',
            {
                xtype:'button',
                text:'Previos',
                handler: function () {
                    var layout = panel.getLayout();
                    if(layout.getPrev()){
                        layout.prev();
                    }
                }
            },
            {
                xtype:'button',
                text:'Next',
                handler: function () {
                    var layout = panel.getLayout();
                    if(layout.getNext()){
                        layout.next();
                    }
                }
            }
        ]
    });
});