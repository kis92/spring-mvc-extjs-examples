Ext.onReady(function () {
    Ext.create('Ext.panel.Panel', {
        renderTo : Ext.getBody(),
        width:'75%',
        height:'75%',
        padding:10,
        title:'dynamic panel',
        layout:{
            // type:'vbox',
            type:'hbox',
            align:'stretch'
        },
        items:[
            {
                xtype:'panel',
                title:'one',
                border:true,
                flex:1
            },
            {
                xtype:'panel',
                title:'two',
                border:true,
                flex:1
            },
            {
                xtype:'panel',
                title:'three',
                border:true,
                flex:2
            }
        ]

    });
});