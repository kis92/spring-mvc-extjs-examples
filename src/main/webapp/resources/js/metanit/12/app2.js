Ext.onReady(function () {
    Ext.define('Company', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'Title',
            type: 'string'
        }, {
            name: 'Date',
            type: 'date',
            dateFormat: 'd-m-Y'
        }, {
            name: 'Value',
            type: 'int'
        }],
        associations: [{
            type: 'hasMany',
            model: 'Manager',
            name: 'founders'
        }]
    });

    Ext.define('Manager', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'FirstName',
            type: 'string'
        }, {
            name: 'LastName',
            type: 'string'
        }]
    });
    var company = Ext.create('Company', {
        Title: 'Microsoft',
        Date: '01-01-1974',
        Value: 30000
    });
    company.founders().add(
        {
            FirstName: 'Bill',
            LastName: 'Gates'
        }, {
            FirstName: 'Paul',
            LastName: 'Allen'
        }
    );
    company.founders().each(function (founder) {
        alert(founder.get('FirstName') + " " + founder.get('LastName'));
    });
});