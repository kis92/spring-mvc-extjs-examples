/*
 var clientRetailConfig = Ext.data.Record.create([
 {name: 'clientRetailId', type: 'int'},
 {name: 'cityId', type: 'string'},
 {name: 'discountRetailId', type: 'string'},
 {name: 'clientRetailCardNumber', type: 'string'},
 {name: 'clientRetailNameFull', type: 'string'},
 {name: 'clientRetailFirstName', type: 'string'},
 {name: 'clientRetailMiddleName', type: 'string'},
 {name: 'clientRetailLastName', type: 'string'},
 {name: 'clientRetailComment', type: 'string'}
 ]);

 var proxy = new Ext.data.HttpProxy({
 api: {
 read: {url: urlPrefix + "/getClientRetail.do"},
 create: {url: urlPrefix + "/addClientRetail.do"},
 update: {url: urlPrefix + "/updateClientRetail.do"},
 destroy: {url: urlPrefix + "/removeClientRetail.do"}
 }
 });

 var reader = new Ext.data.JsonReader({
 totalProperty: 'count',
 successProperty: 'success',
 idProperty: 'clientRetailId',
 root: "clientRetail",
 messageProperty: 'message'
 }, clientRetailConfig);

 var writer = new Ext.data.JsonWriter({
 listful: true,
 encode: true,
 writeAllFields: false
 });

 var clientRetailStore = new Ext.data.Store({
 proxy: proxy,
 idProperty: 'clientRetailId',
 reader: reader,
 writer: writer,
 autoSave: false
 });

*/


Ext.onReady(function () {
    Ext.define('User', {
        extend: Ext.data.Model,
        fields: [
            {name: 'id', type: INTEGER},
            {name: 'name', type: STRING}
        ]
    });

    var myStore = new Ext.data.Store({
        model: User,
        proxy: {
            type: 'ajax',
            url: '/json/12-4.json',
            reader: {
                type: 'json',
                root: 'data.users'
            }
        },
        groupers: [{property: 'name'}]
    });

    var user = new User({
        id: 3,
        name: 'Test'
    });

    //add
    myStore.add(user);

    //insert
    //myStore.insert(0, user);

    //delete
    myStore.remove(user);
    //or
    //myStore.removeAt(0);
    //delete all
    //myStore.removeAll();

    var userFromStore = myStore.getAt(0);
    var userFirst = myStore.first();
    var userLast = myStore.last();
    var storeCount = myStore.count();

    var users = myStore.getRange(0, storeCount);


});