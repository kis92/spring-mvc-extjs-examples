Ext.onReady(function () {
    Ext.define('Person', {
        extend: Ext.data.Model,
        idProperty: 'personId',
        fields: [
            {
                name: 'personId',
                type: Ext.data.Types.INTEGER
            },
            {
                name: 'name',
                type: Ext.data.Types.STRING
            },
            {
                name: 'surname',
                type: Ext.data.Types.STRING
            },
            {
                name: 'birthDate',
                type: Ext.data.Types.DATA,
                dateFormat: 'd-m-Y'
            },
            {
                name: 'salary',
                type: Ext.data.Types.INTEGER
            },
            {
                name: 'married',
                type: Ext.data.Types.BOOLEAN
            }
        ],
        proxy: {
            type: 'ajax',
            api: {
                read: '/read',
                create: '/create',
                update: '/update',
                destroy: '/delete'
            }
        }
    });

    // var personOne = new Person();
    // personOne.load(1, {
    //     callback: function (pers, operation) {
    //         alert(pers.get('name')); // вывод его отдельного свойства
    //     }
    // });
    // console.log(personOne);

    //create
    var testPerson = new Person({
        // personId: 2,//update
        name: 'Eugene3',
        surname: 'Popov3',
        birthDate: '22-05-1984',
        salary: 3002,
        married: true
    });
    testPerson.save();

    //delete
    testPerson.destroy();

    //readAll
    // Ext.Ajax.request({
    //     url:'/readAll',
    //     callback: function (options, success, response) {
    //         console.log(response.responseText);
    //         var objAjax= Ext.decode(response.responseText);
    //         console.log('persons:'+objAjax)
    //     }
    // });
});