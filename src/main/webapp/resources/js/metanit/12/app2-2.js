Ext.onReady(function(){

    Ext.define('Company', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'Title',
            type: 'string'
        },{
            name: 'Date',
            type: 'date',
            dateFormat: 'd-m-Y'
        }, {
            name: 'Value',
            type: 'int'
        }]
    });

    Ext.define('Manager', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'FirstName',
            type: 'string'
        }, {
            name: 'LastName',
            type: 'string'
        }],
        belongsTo: 'Company'
    });
    var manager = Ext.create('Manager', {
        FirstName: 'Bill',
        LastName: 'Gates'
    });
    var company = Ext.create('Company', {
        Title: 'Microsoft',
        Date: '01-01-1974',
        Value: 30000
    });
    manager.setCompany(company);
    manager.getCompany(function(company, operation){
        alert(company.get('Title'));
    });
});