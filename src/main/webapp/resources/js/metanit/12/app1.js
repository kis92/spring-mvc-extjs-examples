Ext.onReady(function () {
    Ext.define('Person', {
        extend: 'Ext.data.Model',
        fields: [
            {
                name: 'Name',
                type: 'string'
            },
            {
                name: 'Surname',
                type: 'string'
            },
            {
                name: 'BirthDate',
                type: 'date',
                dateFormat: 'd-m-Y'
            },
            {
                name: 'Salary',
                type: 'int'
                // ,
                // convert: function (v, record) {
                //     return record.get('Name') + ' получает ' + v
                // }
            },
            {
                name: 'Married',
                type: 'boolean'
            }
        ],
        validations:[
            {
                type:'presence',
                field:'Name'
            },
            {
                type:'length',
                field:'Name',
                min: 2
            },
            {
                type: 'exclusion',
                field: 'Name',
                list: ['Admin', 'Operator']
            },
            {
                type: 'format',
                field: 'Name',
                matcher: /([a-z]+)/
            }
        ]
    });

    var person = new Person({Name: 'Jek', Surname: 'Vorobey', BirthDate: '22-09-2014', Salary: 300, Married: true});
    console.log(person);
    if(person.isValid()){
        alert('Модель прошла валидацию');
    }else{
        var errors = person.validate();
        errors.each(function(error){
            alert("Ошибка в определении поля " + error.field+": "+error.message);
        });
    }

    // new Ext.Panel({
    //     title: 'Person' + person.get('Name') + ' ' + person.get('Surname'),
    //     html: '<p>Имя: ' + person.get('Name') + '</p>' +
    //     '<p>Фамилия: ' + person.get('Surname') + '</p>' +
    //     '<p>Дата рождения: ' + person.get('BirthDate') + '</p>' +
    //     '<p>Женат: ' + (person.get('Married') == true ? 'да' : 'нет') + '</p>' +
    //     '<p>З.П.: ' + person.get('Salary') + '</p>',
    //     width: 200,
    //     height: 200,
    //     renderTo: Ext.getBody(),
    //     style: 'margin-left: 30px;margin-top: 10px;'
    // });

    var formPanel = new Ext.form.Panel({
        title: 'datas',
        width: '75%',
        heightAuto: true,
        renderTo: Ext.getBody(),
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'NAME',
                name: 'Name',
                flex: 1
            },
            {
                xtype: 'textfield',
                fieldLabel: 'SURNAME',
                name: 'Surname',
                flex: 1
            },
            {
                xtype: 'datefield',
                fieldLabel: 'BIRTHDATE',
                name: 'BirthDate',
                flex: 1
            },
            {
                xtype: 'numberfield',
                fieldLabel: 'SALARY',
                name: 'Salary',
                minValue: 0, //prevents negative numbers

                // Remove spinner buttons, and arrow key and mouse wheel listeners
                hideTrigger: true,
                keyNavEnabled: false,
                mouseWheelEnabled: false,
                flex: 1
            },
            {
                xtype: 'checkbox',
                boxLabel: 'MARRIED',
                name: 'Married',
                flex: 1
            }
        ],
        buttons:[
            {
                text:'Fill Jek',
                handler:function () {
                    formPanel.getForm().loadRecord(person);
                }
            }
        ]
    });
});