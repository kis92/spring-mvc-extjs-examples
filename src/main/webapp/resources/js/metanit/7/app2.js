Ext.onReady(function () {
    var window = Ext.create('Ext.window.Window', {
        id: 'myWindow',
        title: 'test window',
        width: '25%',
        height: '30%',
        layout:'border',
        items: [
            {
                id: 'myButton',
                xtype: 'button',
                text: 'push me',
                region: 'north',
                height:'40%',
                handler: function () {
                    console.log("button was pushed");
                    Ext.getCmp('myWindow').hide();

                    Ext.Function.defer(
                        function(){
                            window.show();
                        },
                        10000, this
                    );
                }
            }
        ]
    });
    window.show();
});