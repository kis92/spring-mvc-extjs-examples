Ext.onReady(function () {
    var store = Ext.create('Ext.data.TreeStore', {
        // proxy:{
        //     type:'ajax',
        //     url:'treeData.json'
        // }
        root:{
            text:'countries of sng',
            expand:true,
            children:
            [
                {
                    text:'Russia',
                    children:[
                        {
                            text:'Moscow',
                            leaf:true
                        },
                        {
                            text:'Krym',
                            leaf:true
                        },
                        {
                            text:'Kaliningrad',
                            leaf:true
                        }
                    ],
                    leaf:false,
                    expanded:true
                },
                {
                    text:'Bellorusia'
                },
                {
                    text:'Угорщина'
                }
            ]
        },
        // sorters:[
        //     {
        //         property:'text',
        //         direction:'ASC'
        //     }
        // ]
    });

    Ext.create('Ext.tree.Panel', {
        title:'SNG countries',
        width:'75%',
        height:'75%',
        rootVisible:true,
        renderTo: Ext.getBody(),
        store: store,
        viewConfig: {
            plugins: {
                ptype: 'treeviewdragdrop'
            }
        },
        tbar:[
            {
                xtype:'button',
                text:'ASC',
                handler: function () {
                    store.sort('text', 'ASC');
                }
            },
            '->',
            {
                xtype:'button',
                text:'DESC',
                handler: function () {
                    store.sort('text', 'DESC');
                }
            }

        ]
    });
});