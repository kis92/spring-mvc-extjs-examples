Ext.onReady(function () {
    var myData = [
        [1,'pascal','q'],
        [2,'c','w'],
        [3,'c++','e'],
        [4,'php','r'],
        [5,'java','t']
    ];

    new Ext.panel.Panel({
        width:'75%',
        height:'75%',
        items:[
            {
                xtype:'combobox',
                fieldLabel:'select language',
                store: new Ext.data.SimpleStore({
                    id:0,
                    fields:['myId','myText', 'myRate'],
                    data:myData
                }),
                valueField:'myId',
                displayField:'myText',
                queryMode:'local',
                forceSelection:true,
                listConfig:{
                    getInnerTpl:function () {
                        return '<b>{myText}, rate: {myRate}</b>'
                    }
                }
            }
        ],
        renderTo:Ext.getBody()
    });
});