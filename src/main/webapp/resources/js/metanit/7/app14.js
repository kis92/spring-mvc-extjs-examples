Ext.onReady(function () {
    var myMenu = new Ext.menu.Menu({
        items:[
            {
                text:'js',
                checked:true,
                group:'lang'
            },
            {
                text:'java',
                checked:true,
                group:'lang'
            },
            {
                text:'humans',
                handler:function () {
                    alert('hello');
                }
            }
        ]
    });
    var toolbar = new Ext.toolbar.Toolbar({
        items:[
            {
                text:'button',
                xtype:'button'
            },
            '-',
            {
                xtype:'splitbutton',
                text:'menu button',
                menu: myMenu
            },
            '->',
            {
                xtype:'textfield',
                emptyText:'search',
                name:'searchField'
            }
        ]
    });

    new Ext.panel.Panel({
        title:'panel with toolbar',
        width:'98%',
        height:'75%',
        renderTo:Ext.getBody(),
        dockedItems:[toolbar]
    });
});