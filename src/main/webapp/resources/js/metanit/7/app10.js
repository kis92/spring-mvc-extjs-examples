Ext.onReady(function () {
    new Ext.slider.Slider({
        width: '75%',
        height: '10%',
        renderTo: Ext.getBody(),
        minValue: 0,
        maxValue: 500,
        value: 100,
        increment: 10,
        listeners:{
            change:function(slider, newValue){
                console.log('value = ' + newValue)
            }
        }
    });
});