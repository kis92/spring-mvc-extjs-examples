Ext.onReady(function () {
    Ext.create('Ext.tab.Panel', {
        id:'tabPanel',
        title:'panel',
        width:'75%',
        height:'75%',
        renderTo:Ext.getBody(),
        tabPosition:'left',
        items:[
            {
                title:'C#',
                html:'wpf, ASp'
            },
            {
                title:'java',
                html:'swing, jsp'
            }
        ]
    });

    Ext.create('Ext.button.Button',{
        text:'Add',
        margin:10,
        renderTo:Ext.getBody(),
        handler:function () {
            var tabPanel = Ext.getCmp('tabPanel');
            var tab = tabPanel.add({
                title:'Tab ' + (tabPanel.items.length +1),
                html:'Tab ' + (tabPanel.items.length +1),
            });
            tabPanel.setActiveTab(tab);
        }
    });

    Ext.create('Ext.button.Button',{
        text:'Remove',
        margin:10,
        renderTo:Ext.getBody(),
        handler:function () {
            var tabPanel = Ext.getCmp('tabPanel');
            var tab = tabPanel.getActiveTab();
            tabPanel.remove(tab);
        }
    });
});