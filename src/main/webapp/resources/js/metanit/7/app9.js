Ext.onReady(function () {
    new Ext.Panel({
        width:'75%',
        height:'15%',
        items:[
            {
                xtype:'numberfield',
                fieldLabel:'select number',
                minValue: -5,
                maxValue: 100,
                allowDecimals: true,
                decimalPrecision: 1,
                step: 0.5
            }
        ],
        renderTo:Ext.getBody()
    });
});