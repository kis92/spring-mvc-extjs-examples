Ext.onReady(function () {
    // var childPanel1 = Ext.create('Ext.Panel', {
    //     title:'title1',
    //     html:'inner panel one'
    // });
    //
    // var childPanel2 = Ext.create('Ext.Panel',{
    //     title: 'title2',
    //     html: 'inner panel two'
    // });
    //
    // Ext.create('Ext.container.Viewport',{
    //    items:[childPanel1, childPanel2]
    // });

    //OR

    Ext.create('Ext.container.Viewport',{
        id:'myViewPort',
        items:[
            {
                xtype:'panel',
                title:'panel one',
                html:'inner panel one',
                id:'panelOne'
            },
            {
                xtype: 'panel',
                title:'panel two',
                html:'inner panel two'
            }
        ]
    });
    Ext.getCmp('myViewPort').add({
        xtype:'panel',
        title:'add panel',
        html:'added panel',
        height:'50%',
        width:'50%'
    });
    Ext.getCmp('myViewPort').remove('panelOne');
});