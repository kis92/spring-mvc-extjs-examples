Ext.onReady(function () {

    var formPanel = Ext.create('Ext.Panel', {
        title: 'Форма ввода',
        width: '75%',
        height: '75%',
        bodyPadding: 10,
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Ваше имя:',
                allowBlank: false,
                emptyText: 'введите текст', //подсказка в текстовом поле
                minLength: 3,
                maxLength: 15,
                name: 'name'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Номер счета:',
                allowBlank: false,
                maskRe: /[1-9]/i, //только числа
                name: 'account'
            },
            {
                xtype: 'numberfield',
                fieldLabel: 'Номер счета:',
                allowBlank: false,
                name: 'account2',
                hideTrigger: true,
                keyNavEnabled: false,
                mouseWheelEnabled: false
            }
        ],
        renderTo: Ext.getBody()
    });
});