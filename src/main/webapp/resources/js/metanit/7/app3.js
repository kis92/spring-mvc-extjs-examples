Ext.onReady(function () {
    // Ext.create('Ext.Panel', {
    //     title:'tit',
    //     width:'75%',
    //     height:'75%',
    //     html:'hello',
    //     renderTo: Ext.getBody(),
    //     draggable: 'true',
    //     closable: true,
    //     tbar:['tbar'],
    //     lbar:['lbar'],
    //     rbar:['rbar'],
    //     fbar:['fbar'],
    //     bbar:[
    //         {
    //             html: 'bbar',
    //             titleCollapse: true,
    //             collapsible: true
    //         }
    //     ]
    // });

    Ext.create('Ext.panel.Panel', {
        title: 'Приложение',
        width: '75%',
        height: '75%',
        bodyPadding: 5,
        html: 'Привет мир!',
        style: 'margin: 20px',
        closable: true,
        titleCollapse: true,
        collapsible: true,
        // draggable: 'true',
        // dockedItems: [{
        //     xtype: 'toolbar',
        //     dock: 'top',
        //     items: [{
        //         text: 'Файл',
        //         handler: function(){alert('Привет!');}
        //     }, '->', 'Верхний тулбар']
        // }, {
        //     xtype: 'toolbar',
        //     dock: 'bottom',
        //     items: [{
        //         xtype: 'button',
        //         text: 'Кнопка 1'
        //     }, '-',{
        //         xtype: 'button',
        //         text: 'Кнопка 2'
        //     }, '-',{
        //         xtype: 'button',
        //         text: 'Кнопка 3'
        //     },'->', 'Нижний тулбар']
        // }],
        tools: [
            {type: 'help',
                handler: function(event, toolEl, panel){
                    alert('Help');
                }
            },
            {type: 'gear'},
            {type: 'search'}
        ],
        renderTo: Ext.getBody()
    });
});