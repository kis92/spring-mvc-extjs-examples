Ext.onReady(function () {
    new Ext.Panel({
        title:'html editor',
        width:'75%',
        height:'75%',
        layout:'fit',
        renderTo:Ext.getBody(),
        items:[
            {
                xtype:'htmleditor'
            }
        ]
    });
});