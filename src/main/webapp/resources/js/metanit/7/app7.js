Ext.onReady(function () {
    var radioItems = new Ext.form.RadioGroup(
        {
            //xtype: 'radiogroup',
            fieldLabel: 'genders',
            vertical: true,
            columns: 2,
            items: [
                {
                    boxLabel: 'male',
                    name: 'gender',
                    inputValue: 'MALE'
                },
                {
                    boxLabel: 'female',
                    name: 'gender',
                    inputValue: 'FEMALE'
                },
                {
                    boxLabel: 'male2',
                    name: 'gender',
                    inputValue: 'MALE'
                },
                {
                    boxLabel: 'female2',
                    name: 'gender',
                    inputValue: 'FEMALE'
                }
            ]
        }
    );

    var checkboxItems = new Ext.form.CheckboxGroup({
        columns:2,
        vertical:true,
        fieldLabel:'vegetables',
        items:[
            {
                xtype:'checkbox',
                boxLabel:'CABBAGE',
                inputValue:'cabbage',
                name:'veg',
                checked:'true'
            },
            {
                xtype:'checkbox',
                boxLabel:'CABBAGE2',
                inputValue:'cabbage2',
                name:'veg'
            },
            {
                xtype:'checkbox',
                boxLabel:'CARROT2',
                inputValue:'carrot2',
                name:'veg'
            },
            {
                xtype:'checkbox',
                boxLabel:'CARROT',
                inputValue:'carrot',
                name:'veg'
            }
        ]
    });

    Ext.create('Ext.form.Panel', {
        title: 'radio / checkbox',
        width: '75%',
        autoHeight: true,
        bodyPagging: 5,
        renderTo: Ext.getBody(),
        margin: 5,
        items: [radioItems, checkboxItems]
    });

});