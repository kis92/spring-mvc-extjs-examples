Ext.onReady(function () {
    var dateField = new Ext.form.field.Date({
        fieldLabel:'выбрать дату',
        format:'d/m/Y',
        maxValue: new Date(2016, 11, 21),
        minValue: new Date(2016, 11, 2),
        disabledDates: ['15/12/2016', '17/12/2016'],
        renderTo:Ext.getBody()
    });
});