Ext.onReady(function () {
    Ext.create('Ext.button.Button',{
        text:'Toggle Button',
        enableToggle:true,
        margin:10,
        renderTo:Ext.getBody()
    });

    Ext.create('Ext.button.Button',{
        text:'Menu Button',
        margin:10,
        renderTo:Ext.getBody(),
        menu:[
            {
                text:1
            },
            {
                text:2
            }
        ],
        listeners:{
            click:function () {
                this.setText('clicked');
            },
            mouseover: function () {
                if(!this.mouseover){
                    this.mouseover = true;
                    alert('test');
                }
            }
        }
    });
});