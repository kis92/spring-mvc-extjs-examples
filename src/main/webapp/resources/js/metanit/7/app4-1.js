Ext.onReady(function(){
    var menustore = Ext.create('Ext.data.TreeStore', {
        root: {
            text:"Языки программирования",
            expanded: true,
            children: [{
                text: "C#",
                leaf: true
            },{
                text: "C++",
                leaf: true
            },{
                text: "Java",
                leaf: true
            }]
        }
    });
    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Языки программирования',
        store: menustore,
        width: '50%',
        rootVisible: false,
        region: 'west',
        listeners:{
            itemclick : function(tree, record, item, index, e, options) {
                var nodeText = record.data.text;
                centerPanel.getComponent('txt').setValue(nodeText);
            }
        }
    });
    var centerPanel=Ext.create('Ext.panel.Panel', {
        title: 'Добавить/Удалить узел',
        region: 'center',
        width:'50%',
        bodyPadding:10,
        items:[{
            xtype: 'textfield',
            id: 'txt',
            height: 20,
        },{
            xtype: 'button',
            text:'Добавить',
            handler: function() {
                // получаем введенное в текстовое поле значение
                var newNode=centerPanel.getComponent('txt').getValue();
                // Используем метод appendChild для добавления нового объекта
                tree.getRootNode().appendChild({
                    text: newNode,
                    leaf: true
                });
            }
        },{
            xtype: 'button',
            text:'Удалить',
            handler: function() {
                // получаем выделенный узел для удаления
                var selectedNode=tree.getSelectionModel().getSelection()[0];
                // если таковой имеется, то удаляем
                if(selectedNode)
                {
                    selectedNode.remove(true);
                }
            }
        }]
    });
    Ext.create('Ext.panel.Panel', {
        layout : 'border',
        width: '100%',
        height: '75%',
        padding:10,
        items : [tree, centerPanel],
        renderTo: Ext.getBody()
    });
});