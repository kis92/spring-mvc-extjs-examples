Ext.onReady(function () {
    var passportVType = {
        passport:function (val, field) {
            var passportRegex=/^\d{4}\s\d{6}$/;
            return passportRegex.test(val);
        },
        passportText:'серия и номер паспорта с пробелом',
        passportMask:/[\d\s]/

    };
    Ext.apply(Ext.form.field.VTypes, passportVType);
    var formPanel = new Ext.form.Panel({
        id:'form',
        title:'form panel validation',
        width:'50%',
        height:'50%',
        renderTo: Ext.getBody(),
        items:[
            {
                xtype:'textfield',
                fieldLabel:'e-mail',
                name:'email',
                vtype:'email'
            },
            {
                xtype:'textfield',
                fieldLabel:'URL',
                name:'url',
                vtype:'url'
            },
            {
                xtype:'textfield',
                fieldLabel:'TEXT',
                name:'text',
                minLenght:3,
                maxLenght:7,
                vtype:'alpha'
            },
            {
                xtype:'textfield',
                fieldLabel:'TEXT num',
                name:'text',
                minLenght:3,
                maxLenght:7,
                vtype:'alphanum'
            },
            {
                xtype:'textfield',
                fieldLabel:'Passport numbers',
                name:'passport',
                vtype:'passport'
            }
            ,
            {
                xtype: 'filefield',
                name: 'file',
                fieldLabel: 'Выберите файл: ',
                msgTarget: 'side',
                allowBlank: false
            }
        ],
        buttons: [{
            text: 'Загрузить файл',
            handler: function(){
                var form = this.up('form').getForm();
                if (form.isValid()) {
                    form.submit({
                        url: '/upload',
                        waitMsg: 'Загрузка...',
                        success: function(fp, o){
                            Ext.Msg.alert('Загрузка прошла успешно', 'Файл ' +o.result.file +" загружен");
                        }
                    });
                }
            }
        }]
    });
});