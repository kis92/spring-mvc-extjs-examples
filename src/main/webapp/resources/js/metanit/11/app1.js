Ext.onReady(function () {
    var fillData = {
        login:'test',
        password:'qwerty'
    };

    Ext.define('Person', {
        extend: 'Ext.data.Model',
        fields: [
            'login',
            'password'
        ]
    });
    var personModel = Ext.create('Person', fillData); //заполнение модели данными

    var formPanel = new Ext.form.Panel({
        title: 'auth form',
        width: '95%',
        height: '20%',
        renderTo: Ext.getBody(),
        items: [
            {
                xtype: 'textfield',
                fieldLabel: 'Login',
                name: 'login'
            },
            {
                xtype: 'textfield',
                inputType: 'password',
                fieldLabel: 'Password',
                name: 'password'
            }
        ],
        buttons: [
            {
                text: 'Submit',
                handler: function () {
                    var form = formPanel.getForm().getValues();
                    console.log('login = ' + form.login);
                    console.log('password = ' + form.password);

                    formPanel.getForm().submit({
                        url: '/login',
                        success: function(form, action){
                            console.log(form);
                            Ext.MessageBox.alert('Авторизация пройдена. ',action.result.message);
                            console.log(action.result.message);
                        },
                        failure: function(form, action){
                            console.log(form);
                            Ext.MessageBox.alert('Ошибка авторизации. ',action.result.message);
                        }
                    });
                }
            },
            {
                text: 'Отмена',
                handler: function () {
                    formPanel.getForm().reset();
                }
            },
            {
                text: 'Test',
                handler: function () {
                    //formPanel.getForm().setValues(fillData);
                    formPanel.getForm().loadRecord(personModel);
                }
            }
        ]
    });
});