Ext.onReady(function(){
    Ext.get('mb1').on('click', function (e) {
        var msg =  Ext.get('msg');
        Ext.MessageBox.alert('Alert', 'Your message = ' + msg.dom.innerHTML);
    });
});

var tplData = [
    {
        color : "ffe9e9",
        name : 'Naomi',
        age : 25,
        dob : '02/11/2000',
        cars : ['2109', 'Camry']
    },
    {
        color : "e9e9ff",
        name : 'Smith',
        age : 22,
        dob : '02/11/2008',
        cars : ['2101', 'Camry', '2017']
    }
];

//var myTpl = Ext.create('Ext.XTemplate',[
var myTpl = new Ext.XTemplate(
    '<tpl for=".">',
        '<div style = "background-color: {color}; margin: 10px">',
            '<b>Name:</b> {name} <br/>',
            '<b>Age:</b> {age} <br/>',
            '<b>DOB:</b> {dob} <br/>',
            '<b>Cars:</b>',
                '<tpl for="cars">',
                    '{.}',
                        '<tpl if="this.isCamry(values)">',
                            '<b>(same car)</b>',
                        '</tpl>',
                    '{[(xindex<xcount)?"," : ""]}',
                '</tpl>',
            '<br/>',
        '</div>',
    '</tpl>',
    {
        isCamry : function (car) {
            return car==='Camry';
        }
    }
);
myTpl.compile();
myTpl.append(document.body, tplData);